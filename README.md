# README #

This is a K2 smartforms control that allows user to drag and drop file on the form for uploading to the server.

[K2 Community Market](http://community.k2.com/t5/K2-blackpearl/Drag-and-Drop-Upload-Control/ba-p/63942)


### v1.3 - 20 July 2015 ###
- Built and tested on 4.6.10.
- Added feature to check on maximum file size before uploading to server. Note that the value is in bytes.
- Added feature to restrict file extensions that can be uploaded. Note that when the field is empty, the control allows everything. Multipe file extensions can be added and separated by ';'. e.g. txt;pdf;doc;docx
- Added read-only state for the control User can download, but cannot remove or upload new file.
- fixed disabled state. The control does not allow user to replace the file when the control is in disabled state now.
- Added feature to remove the dotted-border around the control.
﻿
if (typeof DragAndDropControl === 'undefined' || DragAndDropControl == null) DragAndDropControl = {};
if (typeof DragAndDropControl.DragAndDropFile === 'undefined' || DragAndDropControl.DragAndDropFile == null) DragAndDropControl.DragAndDropFile = {};

DragAndDropControl.DragAndDropFile = function (element) {
    if (!(element.id === 'undefined' || element.id == null))
        this._currentID = element.id;
    else if (typeof(element) == 'string') //used by uploadfile function
        this._currentID = element;
    else
        this._currentID = element.CurrentControlId;

    this.initialize();
}

DragAndDropControl.DragAndDropFile.prototype = {
    _currentID: 0,
    _status: "NONE",
    _filePath: "",
    _fileName: "",
    _fileType: "",
    _fileTypeReadable: "",
    _fileSize: "",
    _fileSizeBytes: "",
    _fileSizeMaxBytes: 2048,
    _width: '250px',
    _isEnabled: true,
    _isVisible: true,
    _isReadOnly: false,
    _hasBorder: true,
    _initialized: false,
    _allowedFileExts: "",

    initialize: function() {
        var ctrl = this._getInstance();
        var options = ctrl.data("options");

        //file details
        if(options.status !== undefined)
            this._status = options.status;
        if(options.filepath !== undefined)
            this._filePath = options.filepath;
        if(options.filename !== undefined)
        {
            this._fileName = options.filename;
            var fileExtension = this._fileName.substring(this._fileName.lastIndexOf('.') + 1);
            this._fileTypeReadable = this._getContentTypeFriendlyName(fileExtension);
        }
        if(options.filetype !== undefined)
            this._fileType = options.filetype;
        if(options.filesizebytes !== undefined)
        {
            this._fileSizeBytes = options.filesizebytes;
            this._fileSize = this._getFileSizeString(this._fileSizeBytes, 2);
        }

        if(options.maxfilesize !== undefined)
            this._fileSizeMaxBytes = options.maxfilesize;
        if(options.isenabled !== undefined)
            this._isEnabled = options.isenabled; 
        if(options.isvisible !== undefined)
            this._isVisible = options.isvisible;
        if(options.width !== undefined)
            this._width = options.width;
        if(options.hasborder !== undefined)
            this._hasBorder = options.hasborder;
        if(options.isreadonly !== undefined)
            this._isReadOnly = options.isreadonly;
        if(options.allowedfileexts !== undefined)
            this._allowedFileExts = options.allowedfileexts;


        this._initialized = true;
        if(options.initialized !== undefined)
        {
            if(options.initialized == true)
                return; //do not process this function further.
        }

        options.initialized = true;
        ctrl.data('options', options);

        if(options.maxfilesize !== undefined)
            this._set_maxFileSize(options.maxfilesize);
        if(options.isenabled !== undefined)
            this._set_enabled(options.isenabled); //need to disable in js
    },

    _getInstance: function () {
        var instance = $('#' + this._currentID + '.DragAndDropControl-DragAndDropFile-Control');
        if (checkExists(instance))
        {
            return instance;
        }
        return null;
    },

    _getWrapperInstance: function() {
        var instance = this._getInstance();
        if(instance == null) return null;
        return instance.find('.dragdroppanel');
    },

    _getWrapper: function() {
        var wrapperInst = this._getWrapperInstance();
        if(wrapperInst == null) return null;
        return wrapperInst[0];
    },

    getValue: function (objInfo) {
        if (!checkExists(objInfo))
            return;

        var context = this;
        if(typeof(context) != 'draganddropcontrol.draganddropfile')
            context = new DragAndDropControl.DragAndDropFile(objInfo);

        var fileObject = context._getFileValue();
        if (fileObject.status == "NONE") {
            var returnData = "";
            return returnData
        } else {
            var returnData = fileObject.name;
            if (checkExists(fileObject.path)) {
                returnData = "<collection><object><fields><field name='FileName'><value>" + fileObject.name.xmlEncode() + "</value></field><field name='FilePath'><value>" + fileObject.path.xmlEncode() + "</value></field></fields></object></collection>";
            }
            return returnData;
        }
    },

    _getFileValue: function () {
        return { status: this._status, name: this._fileName, path: this._filePath };
    },

    getDefaultValue: function (objInfo) {
        this.getValue(objInfo);
    },

    //Get Control Properties
    getProperty: function (objInfo)
    {
        var returnVal = undefined;

        if (!checkExists(objInfo))
            return;

        var context = this;
        if(typeof(context) != 'draganddropcontrol.draganddropfile')
            context = new DragAndDropControl.DragAndDropFile(objInfo);
       
        switch (objInfo.property.toLowerCase())
        {
            case "width":
                returnVal = context._width;
                break;
            case "filename":
                returnVal = context._fileName;
                break;
            case "filetype":
                returnVal = context._fileType;
                break;
            case "filetypereadable":
                returnVal = context._fileTypeReadable;
                break;
            case "filesizereadable":
                returnVal = context._fileSize;
                break;
            case "filesizebytes":
                returnVal = context._fileSizeBytes;
                break;
            case "maxfilesize":
                returnVal = context._fileSizeMaxBytes;
                break;
            case "isenabled":
                returnVal = context._isEnabled.toString();
                break;
            case "isvisible":
                returnVal = context._isVisible.toString();
                break;
            case "hasborder":
                returnVal = context._hasBorder.toString();
                break;
            case "isreadonly":
                returnVal = context._isReadOnly.toString();
                break;
            case "allowedfileexts":
                returnVal = context._allowedFileExts;
                break;
        }

        return returnVal;
    },

    /***** end: GET functions *****/


    /***** start: SET functions *****/

    //set a property for the control. note case statement to call helper methods
    setProperty: function (objInfo) {
        if (!checkExists(objInfo))
            return;

        var context = this;
        if(typeof(context) != 'draganddropcontrol.draganddropfile')
            context = new DragAndDropControl.DragAndDropFile(objInfo);
        switch(objInfo.property.toLowerCase())
        {
            case "style":
                context._set_styles(null, objInfo.Value, context._currentID);
                break;
            case "width":
                context._set_width(objInfo.Value);
                break;
            case "isvisible":
                context._set_visible(objInfo.Value);
                break;
            case "isenabled":
                context._set_enabled(objInfo.Value);
                break;
            case "maxfilesize":
                context._set_maxFileSize(objInfo.Value);
                break;
            case "hasborder":
                context._set_border(objInfo.Value);
                break;
            case "isreadonly":
                context._set_readOnly(objInfo.Value);
                break;
            case "allowedfileexts":
                context._set_allowedExts(objInfo.Value);
                break;
            default:
                break;
        }
    },

    setValue: function (objInfo) {
        if (!checkExists(objInfo))
            return;

        var context = this;
        if(typeof(context) != 'draganddropcontrol.draganddropfile')
            context = new DragAndDropControl.DragAndDropFile(objInfo);

        var imgXml = reverseReplaceSpecialChars(objInfo.Value);
        if (imgXml.indexOf("<collection>") > -1) {
            var colXML = parseXML(imgXml);
            var fileNameEl = colXML.selectSingleNode("collection/object/fields/field[@name='FileName']/value");
            var filePathEl = colXML.selectSingleNode("collection/object/fields/field[@name='FilePath']/value");
            context._setFileValue(fileNameEl.text, filePathEl.text, "COMPLETE");
        }
        else
        {
            context._resetControl();
        }
    },

    _setFileValue: function (fileName, filePath, status) {
        this._status = status
        this._filePath = filePath
        this._fileName = fileName
        var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        if(this._allowedFileExts != '')
        {
            if(!fileExtension.match(/^txt(;?)|;txt(;|$)/g))
            {
                this._onError('.' + fileExtension + ' is not an allowed file extension.');
                return;
            }
        }
        var friendlyContentTypeName = this._getContentTypeFriendlyName(fileExtension);

        var ctrl = this._getInstance();
        //set thumbnail
        var imgSrc = this._getImageURL(fileExtension);
        var imgThumbnail = ctrl.find("#" + this._currentID + "_thumbnailImg");
        imgThumbnail.attr('src', imgSrc).bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');
        //set display text
        var displayFileName = this._fileName;
        if (displayFileName.length > 11)
            displayFileName = displayFileName.substring(0, 20) + "...";
        var displayText = displayFileName + "<br/>" + friendlyContentTypeName;
        ctrl.find('.ddText').html(displayText).bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');

        //save to data-options
        var options = ctrl.data('options');
        jQuery.extend(options, {
            'status': this._status,
            'filepath': this._filePath,
            'filename': this._fileName,
            'filetype': this._fileType,
            'filesizebytes': this._fileSizeBytes,
            'filetypereadable': this._fileTypeReadable,
            'filesize': this._getFileSizeString(this._fileSizeBytes, 2)
        });
        ctrl.data('options', options);
    },

    _set_border: function(state) {
        var wrapperInst = this._getWrapperInstance();
        this._hasBorder = this._booleanConverter(state);

        if(this._hasBorder)
        {
            if(!wrapperInst.hasClass('dragdroppanel-border'))
            {
                wrapperInst.addClass('dragdroppanel-border');
            }
        }
        else
        {
            if(wrapperInst.hasClass('dragdroppanel-border'))
            {
                wrapperInst.removeClass('dragdroppanel-border');
            }
        }
        this._getInstance().data('options').hasborder = this._hasBorder;
    },

    //helper method to set visibility
    _set_allowedExts: function (exts) {
        this._allowedFileExts = exts;
        //save to data-options
        this._getInstance().data('options').allowedfileexts = exts;
    },

    //helper method to set visibility
    _set_visible: function (state) {
        this._isVisible = this._booleanConverter(state);
        var displayValue = this._isVisible ? "block" : "none";
        this._getWrapper().style.display = displayValue;
        //save to data-options
        this._getInstance().data('options').isvisible = this._isVisible;
    },

    //helper method to set visibility
    _set_readOnly: function (state) {
        if(!this._isEnabled)
        {
            //already disabled. just ignore
            return;
        }    
        
        //just remove the "x" button if true and do not allow drop
        this._isReadOnly = this._booleanConverter(state);
        var ctrl = this._getInstance();
        // "x" icon
        if (this._fileName != '')
        {
            if (this._isReadOnly) {
                ctrl.find('#' + this._currentID + '_closeIcon').unbind('click').hide();
            }
            else {
                ctrl.find('#' + this._currentID + '_closeIcon').unbind('click').bind('click', this, this._onCloseIconClick).show();
            }
        }
        //save to data-options
        ctrl.data('options').isreadonly = this._isReadOnly;
    },

    //helper method to set control "enabled" state
    _set_enabled: function (state) {
        this._isEnabled = this._booleanConverter(state);
        var ctrl = this._getInstance();
        var wrapper = this._getWrapper();
        if (!this._isEnabled) {
            wrapper.style.backgroundColor = '#e5e5e5';
            if (this._fileName != '') {
                ctrl.find("#" + this._currentID + "_thumbnailImg").unbind('click').removeClass('ddPointerCursor');
                ctrl.find('.ddText').unbind('click').removeClass('ddPointerCursor');

                if(!this._isReadOnly)
                    ctrl.find('#' + this._currentID + '_closeIcon').unbind('click').hide();
            }
        }
        else
        {
            wrapper.style.backgroundColor = '';
            if (this._fileName != '') {
                ctrl.find("#" + this._currentID + "_thumbnailImg").unbind('click').bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');
                ctrl.find('.ddText').unbind('click').bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');

                if(!this._isReadOnly)
                    ctrl.find('#' + this._currentID + '_closeIcon').unbind('click').bind('click', this, this._onCloseIconClick).show();
            }
        }

        if(this._initialized)
        {
            //save to data-options
            ctrl.data('options').isenabled =  this._isEnabled;
        }
    },

    _set_styles: function (wrapper, styles, target) {
        var isRuntime = (wrapper === null);
        var options = {};
        var element = isRuntime ? $('#' + target) : wrapper.find('.DragAndDropControl.DragAndDropFile');

        jQuery.extend(options, {
            "border": element,
            "background": element,
            "margin": element,
            "padding": element,
            "font": element,
            "horizontalAlign": element
        });

        StyleHelper.setStyles(options, styles);
    },

    _set_width: function (newWidth) {
        var handler = this._getWrapper();
        var value = newWidth;
        if (value == '' || value.toLowerCase() == 'auto') {
            handler.style.width = 'auto';
            this._width = value;
        }
        else {
            var unit = value.indexOf('%') == -1 ? 'px' : '%';
            value = value.toLowerCase().replaceAll('px', '').replaceAll('%', '');

            value = isNaN(parseInt(value)) ? parseFloat(value) : parseInt(value);

            if (!isNaN(value)) {
                handler.style.width = value + unit;
                this._width = value + unit;
            }
        }
    },

    _set_maxFileSize: function (newSizeInBytes) {
        var value = newSizeInBytes;
        if (value == null || value === 'undefined' || value < 1)
            value = 2048;

        this._fileSizeMaxBytes = value;
        if(this._initialized)
            this._getInstance().data('options').maxfilesize = value; //save back to data-options
    },

    /***** end: SET functions *****/

    /***** start: file *****/

    uploadFile: function (dataTransfer) {
        var file = dataTransfer.files[0];

        if(this._allowedFileExts != '')
        {
            var fileExtension = file.name.substring(file.name.lastIndexOf('.') + 1);
            var regex = new RegExp("^" + fileExtension + "(;?)|;" + fileExtension + "(;|$)", "g");
            if(!this._allowedFileExts.match(regex))
            {
                this._onError(fileExtension + ' is not an allowed file extension.');
                return;
            }
        }

        this._fileName = file.name;
        this._fileType = file.type;
        this._fileSize = this._getFileSizeString(file.size, 2);
        this._fileSizeBytes = file.size;
        this._status = "BUSY";

        if(this._fileSizeBytes > this._fileSizeMaxBytes)
        {
            this._onError("Unable to upload file greater than " + this._getFileSizeString(this._fileSizeMaxBytes, 2) + ".");
            this._resetControl();
            return;
        }
        else if (this._fileSizeBytes < 1)
        {
            this._onError("Unable to upload file lesser than 1 byte.");
            this._resetControl();
            return;
        }

        var data = new FormData();
        data.append(this._fileName, file);

        //Allow for anonymous access:
        if (checkExists(__runtimeAnonTokenName) && checkExists(__runtimeAnonToken)) {
            var settings = {};
            settings.headers = {};
            settings.headers[__runtimeAnonTokenName] = __runtimeAnonToken;
            //Append anonymous token to header of all ajax calls:
            jQuery.ajaxSetup(settings);
        }

        $.ajax(
        {
            context: this,
            type: 'POST',
            url: '/Runtime/Utilities/FileHandler.ashx?fn=' + this._fileName + '&' + (new Date()).getTime(),
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            async: false
        })
        .done(function (data) {
            var filePath = '';
            var responseArray = data.split("|");
            if (checkExistsNotEmpty(responseArray)) {
                filePath = responseArray[0];
            }

            this._finishUploadFile(filePath, this._fileName, this._fileSize);

            raiseEvent(this._currentID, 'Control', 'FileAdded');
        })
        .fail(function (data) {
            this._onError("Failed to upload file. - " + data);
        });
    },

    _finishUploadFile: function (filePath, fileName, fileLength) {
        var ctrl = this._getInstance();
        this._filePath = filePath
        var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        this._fileTypeReadable = this._getContentTypeFriendlyName(fileExtension);
        //set thumbnail
        var imgSrc = this._getImageURL(fileExtension);
        var imgThumbnail = ctrl.find("#" + this._currentID + "_thumbnailImg");
        imgThumbnail.attr('src', imgSrc).bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');
        //set display text
        var displayFileName = this._fileName;
        if (displayFileName.length > 11)
            displayFileName = displayFileName.substring(0, 20) + "...";
        var displayText = displayFileName + "<br/>(" + this._fileTypeReadable + ")<br/>" + fileLength;
        ctrl.find('.ddText').html(displayText).bind('click', this, this._onDownloadClick).addClass('ddPointerCursor');
        //close icon
        var closeIcon = ctrl.find('#' + this._currentID + '_closeIcon');
        closeIcon.show();
        closeIcon.find('.ddCloseIconImg').unbind('click').bind('click', this, this._onCloseIconClick);
        //set status
        this._status = "COMPLETE";

        //save to data-options
        var options = ctrl.data('options');
        jQuery.extend(options, {
            'status': this._status,
            'filepath': this._filePath,
            'filename': this._fileName,
            'filetype': this._fileType,
            'filesizebytes': this._fileSizeBytes,
            'filetypereadable': this._fileTypeReadable,
            'filesize': this._getFileSizeString(this._fileSizeBytes, 2)
        });
        ctrl.data('options', options);
    },

    _getFileSizeString: function (length, decimalSpaces) {
        var size = "";
        if (length < 1024) {
            size = length + " bytes";
        }
        else {
            var divider = 1024;
            var fLength = parseFloat(length) / divider;

            if (fLength < divider) {
                size = fLength.toFixed(2) + " KB";
            }
            else {
                fLength = parseFloat(length) / divider / divider;

                size = fLength.toFixed(decimalSpaces) + " MB";
            }
        }
        return size;
    },

    _onCloseIconClick: function (e) {
        e.preventDefault();
        var obj = new DragAndDropControl.DragAndDropFile(e.data._currentID);
        obj._resetControl();
    },

    _resetControl: function () {
        var ctrl = this._getInstance();
        //reset thumbnail
        var imgSrc = applicationRoot + '<%= WebResource("DragAndDropControl.Resources.noFile.png")%>'
        var imgThumbnail = ctrl.find("#" + this._currentID + "_thumbnailImg");
        imgThumbnail.attr('src', imgSrc).removeClass('ddPointerCursor').unbind('click');
        //reset text
        ctrl.find('.ddText').html("<span>Drop file here</span>").removeClass('ddPointerCursor').unbind('click');
        //remove close icon
        ctrl.find('#' + this._currentID + '_closeIcon').unbind('click').hide();

        this._fileName = this._filePath = this._fileSize = this._fileSizeBytes = this._fileType = this._fileTypeReadable = '';
        
        //reset data-options
        var options = ctrl.data('options');
        jQuery.extend(options, {
            'filename': '',
            'filepath': '',
            'filesizebytes': '',
            'filetype': ''
        });
        ctrl.data('options', options);
    },

    _onDownloadClick: function (e) {
        e.preventDefault();
        var obj = new DragAndDropControl.DragAndDropFile(e.data._currentID);
        if (obj._status == "COMPLETE") {
            openFile(obj._filePath, "Path", "file");
        }
    },

    _onError: function (message) {
        popupManager.showError(message);
    },

    _getContentTypeFriendlyName: function (fileExtension) {
        var friendlyName = '';

        switch (fileExtension.toLowerCase()) {
            case 'csv':
                {
                    friendlyName = 'Comma Seperated Values File';
                    break;
                }
            case 'html':
            case 'htm':
                {
                    friendlyName = 'HTML Document';
                    break;
                }
            case 'chm':
                {
                    friendlyName = 'Compiled HTML Help File';
                    break;
                }
            case 'zip':
                {
                    friendlyName = 'Compressed Folder';
                    break;
                }
            case 'xsn':
                {
                    friendlyName = 'Microsoft Office InfoPath Form Template';
                    break;
                }
            case 'pdf':
                {
                    friendlyName = 'PDF File';
                    break;
                }
            case 'xls':
            case 'xlsx':
                {
                    friendlyName = 'Microsoft Office Excel Worksheet';
                    break;
                }
            case 'doc':
            case 'docx':
                {
                    friendlyName = 'Microsoft Office Word Document';
                    break;
                }
            case 'txt':
                {
                    friendlyName = 'Text Document';
                    break;
                }
            case 'xml':
                {
                    friendlyName = 'XML Document';
                    break;
                }
            case 'one':
                {
                    friendlyName = 'Microsoft Office OneNote Section';
                    break;
                }
            case 'ppt':
            case 'pptx':
                {
                    friendlyName = 'Microsoft Office PowerPoint Presentation';
                    break;
                }
            case 'png':
                {
                    friendlyName = 'PNG Image';
                    break;
                }
            case 'bmp':
                {
                    friendlyName = 'Bitmap Image';
                    break;
                }
            case 'jpeg':
            case 'pjpeg':
            case 'jpg':
                {
                    friendlyName = 'JPEG Image';
                    break;
                }
            case 'gif':
                {
                    friendlyName = 'GIF Image';
                    break;
                }
            case 'tif':
                {
                    friendlyName = 'TIF Image';
                    break;
                }
            case 'mp3':
                {
                    friendlyName = 'MPEG-1 Audio Layer 3';
                    break;
                }
            case 'wav':
                {
                    friendlyName = 'Waveform Audio File';
                    break;
                }
            case 'wmv':
                {
                    friendlyName = 'Windows Media Video';
                    break;
                }
            case 'wma':
                {
                    friendlyName = 'Windows Media Audio';
                    break;
                }
            case 'm4a':
                {
                    friendlyName = 'MPEG 4 Audio File';
                    break;
                }
            case 'm4p':
                {
                    friendlyName = 'Protected AAC File';
                    break;
                }
            case 'avi':
                {
                    friendlyName = 'Audio Video Interleave';
                    break;
                }
            case 'mpeg':
                {
                    friendlyName = 'Moving Picture Experts Group';
                    break;
                }
            case 'flv':
                {
                    friendlyName = 'Flash Video';
                    break;
                }
            case 'mp4':
                {
                    friendlyName = 'Moving Picture Expert Group-4';
                    break;
                }
            default:
                {
                    friendlyName = 'File';
                    break;
                }
        }

        return friendlyName;
    },

    _getImageURL: function (fileExtension) {
        var imageURL = '';

        switch (fileExtension.toLowerCase()) {
            case 'csv':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.CSVGeneric.png")%>;
                    break;
                }
            case 'html':
            case 'htm':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.HTMLGeneric.png")%>;
                    break;
                }
            case 'chm':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.CHMGeneric.png")%>;
                    break;
                }
            case 'zip':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.ZIPGeneric.png")%>;
                    break;
                }
            case 'xsn':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.XSNGeneric.png")%>;
                    break;
                }
            case 'pdf':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.PDFGeneric.png")%>;
                    break;
                }
            case 'xls':
            case 'xlsx':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.XLSGeneric.png")%>;
                    break;
                }
            case 'doc':
            case 'docx':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.DOCGeneric.png")%>;
                    break;
                }
            case 'txt':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.TXTGeneric.png")%>;
                    break;
                }
            case 'xml':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.XMLGeneric.png")%>;
                    break;
                }
            case 'one':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.ONEGeneric.png")%>;
                    break;
                }
            case 'ppt':
            case 'pptx':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.PPTGeneric.png")%>;
                    break;
                }
            case 'png':
            case 'bmp':
            case 'jpeg':
            case 'pjpeg':
            case 'jpg':
            case 'gif':
            case 'tif':
            case 'ico':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.BMPGeneric.png")%>;
                    break;
                }
            case 'mp3':
            case 'wma':
            case 'm4a':
            case 'm4p':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.MusicGeneric.png")%>;
                    break;
                }
            case 'wav':
            case 'wmv':
            case 'avi':
            case 'mpeg':
            case 'flv':
            case 'mp4':
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.VideoGeneric.png")%>;
                    break;
                }
            default:
                {
                    imageURL = <%= WebResource("DragAndDropControl.Resources.File32flat.png")%>;
                    break;
                }
        }

        return applicationRoot + imageURL;
    },

    /***** end: file *****/

    validate: function (objInfo) {
        //alert("validate for control " + objInfo.CurrentControlId);
    },

    execute: function (objInfo) {
        if (!checkExists(objInfo))
            return;

        var method = objInfo.methodName;
        if ((checkExistsNotEmpty(method)))
        {
            var context = this;
            if(typeof(context) != 'draganddropcontrol.draganddropfile')
                context = new DragAndDropControl.DragAndDropFile(objInfo);

            switch (method)
            {
                case "focus":
                    context.focus();
                    break;
                case "clearfile":
                    context._resetControl();
                    break;
            }
        }
    },

    focus: function ()
    {
        $('html,body').animate({
            scrollTop: $("#"+ this._currentID).offset().top},
        'fast');
    },

    _unbindDragDrop: function()
    {
        this._getInstance()
            .unbind('dragover.Control')
            .unbind('dragleave.Control')
    },

    _booleanConverter: function (optionValue)
    {
        var trueValues = ['true', 'yes', '1'];
        return trueValues.contains((optionValue + "").toLowerCase());
    }
};

$(document).ready(function () {
    //handle on change
//    $(document).delegate(".SFC.DragAndDropControl-DragAndDropFile-Control", "change.Control", function (e) {
//        raiseEvent(this.id, 'Control', 'OnChange');
//    });

    DragAndDropControlAddDelegate();
});


function DragAndDropControlAddDelegate() {
    //drag over event
    $(document).delegate(".SFC.DragAndDropControl-DragAndDropFile-Control", "dragover.Control", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var context = new DragAndDropControl.DragAndDropFile(this.id);
        if(context._isEnabled && !context._isReadOnly)
        {
            context._getWrapperInstance().addClass('hover');
        }
    });
    //drag leave event
    $(document).delegate(".SFC.DragAndDropControl-DragAndDropFile-Control", "dragleave.Control", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var context = new DragAndDropControl.DragAndDropFile(this.id);
        if(context._isEnabled && !context._isReadOnly)
        {
            context._getWrapperInstance().removeClass('hover');
        }
    });
    //drag drop event
    $(document).delegate(".SFC.DragAndDropControl-DragAndDropFile-Control", "drop.Control", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var context = new DragAndDropControl.DragAndDropFile(this.id);
        if(context._isEnabled && !context._isReadOnly)
        {
            context._getWrapperInstance().removeClass('hover');
            context.uploadFile(e.originalEvent.dataTransfer);
        }
    });
}

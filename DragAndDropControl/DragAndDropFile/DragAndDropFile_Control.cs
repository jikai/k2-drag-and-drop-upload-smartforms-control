﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using SourceCode.Forms.Controls.Web;
using System.IO;

#region web resources
[assembly: WebResource("DragAndDropControl.DragAndDropFile.DragAndDropFile_Script.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("DragAndDropControl.DragAndDropFile.DragAndDropFile_Stylesheet.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("DragAndDropControl.Resources.noFile.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.BMPGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.CHMGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.Close_icon.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.CSVGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.DOCGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.File32flat.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.HTMLGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.MusicGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.ONEGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.PDFGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.PPTGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.TXTGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.VideoGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.XLSGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.XMLGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.XSNGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.ZIPGeneric.png", "image/png")]
[assembly: WebResource("DragAndDropControl.Resources.DragDropIcon.png", "image/png")]
#endregion

namespace DragAndDropControl.DragAndDropFile
{
    [ControlTypeDefinition("DragAndDropControl.DragAndDropFile.DragAndDropFile_Definition.xml")]
    [ClientScript("DragAndDropControl.DragAndDropFile.DragAndDropFile_Script.js")]
    [ClientCss("DragAndDropControl.DragAndDropFile.DragAndDropFile_Stylesheet.css")]
    public class Control : BaseControl
    {
        #region html controls
        private ClientScriptManager cm;
        private Type rs;
        private Panel ctrlPanel;
        private System.Web.UI.WebControls.Label ctrlText;
        private Panel ctrlThumbnailPanel;
        private System.Web.UI.WebControls.Image ctrlThumbnail;
        private Panel ctrlTextPanel;
        private Panel ctrlOverlay;
        private System.Web.UI.WebControls.Image closeIcon;
        private System.Web.UI.WebControls.HyperLink focusLink;
        #endregion

        #region Control Properties

        //IsBorderless property
        public bool HasBorder
        {
            get
            {
                int retVal = this.GetOption<int>("hasborder", 0);
                return (retVal == 1);
            }
            set
            {
                this.SetOption<int>("hasborder", value ? 1 : 0, 0);
            }
        }

        //control width
        public string Width
        {
            get
            {
                return this.GetOption<string>("width", "250px"); //default to 250px
            }
            set
            {
                string newWidth = string.Empty;
                if(!string.IsNullOrEmpty(value) && !(value.EndsWith("%") || value.EndsWith("px")))
                {
                    newWidth = value + "px";
                }
                this.SetOption<string>("width", newWidth, "100%");
            }
        }

        public string MaxFileSize
        {
            get
            {
                return this.GetOption<string>("maxfilesize", "2048");
            }
            set
            {
                this.SetOption<string>("maxfilesize", value, "2048");
            }
        }

        public string AllowedFileExts
        {
            get
            {
                return this.GetOption<string>("allowedfileexts", string.Empty);
            }
            set
            {
                this.SetOption<string>("allowedfileexts", value, string.Empty);
            }
        }

        #endregion

        #region Constructor
        public Control()
            : base("div")
        {
            this.cm = this.Page.ClientScript;
            this.rs = base.GetType();
            ((SourceCode.Forms.Controls.Web.Shared.IControl)this).DesignFormattingPaths.Add("stlyecss", "DragAndDropControl.DragAndDropFile.DragAndDropFile_Stylesheet.css"); 
        }
        #endregion

        #region Control Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void CreateChildControls()
        {
            base.EnsureChildControls();
            switch (base.State) 
            {
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime:
                    this.ID = Guid.NewGuid().ToString();
                    break;
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Preview:
                    break;
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime:
                    //this.Attributes.Add("enabled", this.Enabled.ToString());
                    //this.Attributes.Add("visible", this.Visible.ToString());
                    break;
            }            
            //ctrl item
            ctrlPanel = new Panel();
            //Thumbnail panel
            ctrlThumbnailPanel = new Panel();
            ctrlThumbnailPanel.CssClass = "ddThumbnailPanel";
            //Thumbnail Image
            ctrlThumbnail = new System.Web.UI.WebControls.Image();
            ctrlThumbnail.CssClass = "ddThumbnail";
            ctrlThumbnail.ImageUrl = this.cm.GetWebResourceUrl(this.rs, "DragAndDropControl.Resources.noFile.png");
            ctrlThumbnail.Style.Clear();
            //set control text panel
            ctrlTextPanel = new Panel();
            ctrlTextPanel.CssClass = "ddTextPanel";
            //control text label
            ctrlText = new System.Web.UI.WebControls.Label();
            ctrlText.CssClass = "ddText";
            ctrlText.Text = "Drop file here";
            //control overlay
            ctrlOverlay = new Panel();
            ctrlOverlay.CssClass = "ddCtrlOverlay";
            ctrlOverlay.Style.Add(HtmlTextWriterStyle.Display, "none");
            closeIcon = new System.Web.UI.WebControls.Image();
            closeIcon.CssClass = "ddCloseIconImg";
            closeIcon.ImageUrl = this.cm.GetWebResourceUrl(this.rs, "DragAndDropControl.Resources.Close_icon.png");
            ctrlOverlay.Controls.Add(closeIcon);
            base.CreateChildControls();
        }
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderContents(writer);
            
            ctrlPanel.ID = this.ID + "_panel";
            ctrlPanel.Style.Add(HtmlTextWriterStyle.Width, this.Width);

            string cssClass = "dragdroppanel ";
            if (this.HasBorder)
            {
                cssClass += "dragdroppanel-border ";
            }
            if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime)
            {
                if (!this.Visible)
                {
                    ctrlPanel.Style.Add(HtmlTextWriterStyle.Display, "none");
                }
            }
            ctrlPanel.Attributes.Add("class", cssClass);
            ctrlPanel.RenderBeginTag(writer);
            //draw thumbnail panel
            ctrlThumbnail.ID = this.ID + "_thumbnailImg";
            ctrlThumbnailPanel.Controls.Add(ctrlThumbnail);
            ctrlThumbnailPanel.RenderControl(writer);
            //text panel
            ctrlTextPanel.RenderBeginTag(writer);
            //text table
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ddTextTable");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ddTextTableTD");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            //draw text
            ctrlText.RenderControl(writer);
            writer.RenderEndTag(); //td
            writer.RenderEndTag(); //tr
            writer.RenderEndTag(); //table
            ctrlTextPanel.RenderEndTag(writer); //text panel
            //overlay
            ctrlOverlay.ID = this.ID + "_closeIcon";
            ctrlOverlay.RenderControl(writer);
            ctrlPanel.RenderEndTag(writer); //ctrl panel
        }
        #endregion
    }
}
